import { writable } from "svelte/store";

export interface User{
    picture: {thumbnail: string};
    name: {first: string; last: string}
}

export interface FetchUserResult{
    isLoading: import("svelte/store").Writable<boolean>;
    error: import("svelte/store").Writable<Error | undefined>;
    data: import("svelte/store").Writable<User[] | undefined>;
}

const useFetchUsers = (): FetchUserResult => {
   const data = writable<User[] | undefined>(undefined);
   const error = writable<Error | undefined>(undefined);
   const isLoading = writable<boolean>(false);

   async function fetchData(): Promise<void>{
      isLoading.set(true);
      try{
        const response = await fetch("https://randomuser.me/api/?results=3");
        const {results: users} = await response.json();
        data.set(users)
        error.set(undefined)
      } catch (err: any) {
        data.set(undefined);
        error.set(err);
      }
      isLoading.set(false)
   }

   fetchData();

   return {isLoading, error, data}
}


export default useFetchUsers