import { writable } from "svelte/store";

export interface AppState{
    count: number;
}

const initialState: AppState = {
    count: 0,
}

export const appStore = writable(initialState);

export const increment = () => {
   appStore.update((state) => ({
       ...state,
       count: state.count + 1
   }))
}

export const decrement = () => {
    appStore.update((state) => ({
        ...state,
        count: state.count -1
    }))
}




