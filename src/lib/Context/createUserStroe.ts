import { writable, type Subscriber, type Writable } from "svelte/store";

// Định nghĩa User interface 
interface User{
    id: number;
    username: string;
    email: string;
}

const createUserStore = (initialData: User): {
    subscribe: (this: void, run: Subscriber<User>) => () => void;
    updateUsername: (newUsername: string) => void;
} => {
   const userStore: Writable<User> = writable(initialData)

   return {
      subscribe: userStore.subscribe,
      updateUsername: (newUsername: string) => {
          userStore.update((userData) => ({
            ...userData,
            username: newUsername
          }))
      }
   }
}

export default createUserStore